package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "user-remove-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "User removing by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter id:");
        @NotNull final String userId = TerminalUtil.nextLine();
        @NotNull final Result result = serviceLocator.getAdminEndpoint().removeUserById(serviceLocator.getSession(), userId);
        printResult(result);
    }

}
