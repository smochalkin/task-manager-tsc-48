package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter project name: ");
        @NotNull final String projectName = TerminalUtil.nextLine();
        @NotNull final Result result = serviceLocator.getProjectEndpoint()
                .removeProjectByName(serviceLocator.getSession(), projectName);
        printResult(result);
    }

}
