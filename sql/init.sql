CREATE SCHEMA tm;

SET search_path to tm;

CREATE TABLE IF NOT EXISTS tm_user (
    id VARCHAR(50) NOT NULL,
    login VARCHAR(50) NOT NULL,
    password_hash VARCHAR(100) NOT NULL,
    role VARCHAR(50) NOT NULL,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    middle_name VARCHAR(100),
    email VARCHAR(100),
    lock boolean DEFAULT false,
    PRIMARY KEY (id),
    UNIQUE(login)
);

INSERT INTO tm_user (id, login, password_hash,role,email) VALUES
('89f99f97-6624-4085-903f-06c87aa87cc8', 'admin', 'f7cdf2fc9225dfac70a063b202a03e34','ADMIN','admin@admin.ru'),
('c73cb725-6589-4bf2-bac5-9b4b51b5952a', 'user', 'fa511bd723f223e4c7a2dfa5109f1a2a','USER','user@user.ru');


CREATE TABLE IF NOT EXISTS tm_session (
    id VARCHAR(50) NOT NULL,
    user_id VARCHAR(50) NOT NULL,
    time_stamp INT8 NULL,
    signature VARCHAR(100),
    PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS tm_project (
    id VARCHAR(50) NOT NULL,
    user_id VARCHAR(50) NOT NULL,
    status VARCHAR(50) NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    start_date TIMESTAMP,
    end_date TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS tm_task (
    id VARCHAR(50) NOT NULL,
    user_id VARCHAR(50) NOT NULL,
    status VARCHAR(50) NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    start_date TIMESTAMP,
    end_date TIMESTAMP,
    project_id VARCHAR(50),
    PRIMARY KEY (id)
);
