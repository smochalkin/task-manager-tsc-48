package ru.smochalkin.tm.exception.empty;

import ru.smochalkin.tm.exception.AbstractException;

public final class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}
