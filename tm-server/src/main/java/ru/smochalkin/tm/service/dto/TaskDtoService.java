package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.dto.ITaskDtoRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class TaskDtoService extends AbstractBusinessDtoService<TaskDto> implements ITaskService {

    public TaskDtoService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public void create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final TaskDto project = new TaskDto();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = new TaskDtoRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = new TaskDtoRepository(entityManager);
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public int getCount() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            return projectRepository.getCount();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            projectRepository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<TaskDto> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            return projectRepository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll(@NotNull final String userId, @NotNull final String sort) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<TaskDto> comparator = sortType.getComparator();
            return projectRepository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            return projectRepository.findByIdAndUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            return projectRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            return projectRepository.findByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            projectRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            projectRepository.removeByIdAndUserId(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            projectRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @Nullable final TaskDto project = findByIndex(userId, index);
            if (project == null) throw new EntityNotFoundException();
            @NotNull final ITaskDtoRepository repository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final TaskDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        try {
            project.setName(name);
            project.setDescription(desc);
            @NotNull final ITaskDtoRepository repository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @Nullable final String desc
    ) {

        @Nullable final TaskDto entity = findByIndex(userId, index);
        if (entity == null) throw new EntityNotFoundException();
        entity.setName(name);
        entity.setDescription(desc);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String strStatus
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final TaskDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        try {
            Status status = Status.getStatus(strStatus);
            Map<String, Date> dateMap = prepareDates(status);
            project.setStatus(status);
            project.setStartDate(dateMap.get("start_date"));
            project.setEndDate(dateMap.get("end_date"));
            @NotNull final ITaskDtoRepository repository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String strStatus
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final TaskDto project = findByName(userId, name);
        if (project == null) throw new EntityNotFoundException();
        try {
            Status status = Status.getStatus(strStatus);
            Map<String, Date> dateMap = prepareDates(status);
            project.setStatus(status);
            project.setStartDate(dateMap.get("start_date"));
            project.setEndDate(dateMap.get("end_date"));
            @NotNull final ITaskDtoRepository repository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String strStatus
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final TaskDto project = findByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        try {
            Status status = Status.getStatus(strStatus);
            Map<String, Date> dateMap = prepareDates(status);
            project.setStatus(status);
            project.setStartDate(dateMap.get("start_date"));
            project.setEndDate(dateMap.get("end_date"));
            @NotNull final ITaskDtoRepository repository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isNotIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) return true;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository projectRepository = new TaskDtoRepository(entityManager);
            return index >= projectRepository.getCountByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    private Map<String, Date> prepareDates(@NotNull final Status status) {
        Map<String, Date> map = new HashMap<>();
        switch (status) {
            case IN_PROGRESS:
                map.put("start_date", new Date());
                map.put("end_date", null);
                break;
            case COMPLETED:
                map.put("start_date", null);
                map.put("end_date", new Date());
                break;
            case NOT_STARTED:
                map.put("start_date", null);
                map.put("end_date", null);
        }
        return map;
    }

}
