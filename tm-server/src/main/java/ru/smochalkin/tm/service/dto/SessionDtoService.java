package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.dto.ISessionDtoRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.ISessionService;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.empty.EmptyPasswordException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.UserIsLocked;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.repository.dto.SessionDtoRepository;
import ru.smochalkin.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public class SessionDtoService extends AbstractDtoService<SessionDto> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionDtoService(
            @NotNull IConnectionService connectionService,
            @NotNull ServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            sessionRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDto> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<SessionDto> findAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public final SessionDto open(@NotNull final String login, @NotNull final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final UserDto userDto = serviceLocator.getUserService().findByLogin(login);
        if (userDto.isLock()) throw new UserIsLocked();
        @NotNull final String secret = serviceLocator.getPropertyService().getPasswordSecret();
        @NotNull final Integer iteration = serviceLocator.getPropertyService().getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(password, secret, iteration);
        if (!userDto.getPasswordHash().equals(hash)) throw new AccessDeniedException();
        @NotNull final SessionDto sessionDto = new SessionDto(userDto.getId());
        sign(sessionDto);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            sessionRepository.add(sessionDto);
            entityManager.getTransaction().commit();
            return sessionDto;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void close(@NotNull final SessionDto session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
            sessionRepository.removeById(session.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void closeAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void validate(@Nullable final SessionDto sessionDto) {
        if (sessionDto == null) throw new AccessDeniedException();
        if (isEmpty(sessionDto.getSignature())) throw new AccessDeniedException();
        if (isEmpty(sessionDto.getUserId())) throw new AccessDeniedException();
        @Nullable final SessionDto temp = sessionDto.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = sessionDto.getSignature();
        @Nullable final SessionDto sessionDtoTarget = sign(temp);
        @Nullable final String signatureTarget = sessionDtoTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);
        if (sessionRepository.findById(sessionDto.getId()) == null) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDto sessionDto, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(sessionDto);
        final @Nullable UserDto userDto = serviceLocator.getUserService().findById(sessionDto.getUserId());
        if (userDto == null) throw new AccessDeniedException();
        if (!userDto.getRole().equals(role)) throw new AccessDeniedException();
    }

    @NotNull
    public SessionDto sign(@NotNull final SessionDto sessionDto) {
        sessionDto.setSignature(null);
        @NotNull final IPropertyService ps = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(sessionDto, ps.getSignSecret(), ps.getSignIteration());
        sessionDto.setSignature(signature);
        return sessionDto;
    }

    @Override
    @SneakyThrows
    public int getCount() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository userRepository = new SessionDtoRepository(entityManager);
            return userRepository.getCount();
        } finally {
            entityManager.close();
        }
    }

}