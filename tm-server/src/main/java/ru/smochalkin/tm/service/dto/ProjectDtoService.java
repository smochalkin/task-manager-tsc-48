package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.dto.IProjectDtoRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.repository.dto.ProjectDtoRepository;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class ProjectDtoService extends AbstractBusinessDtoService<ProjectDto> implements IProjectService {

    public ProjectDtoService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public void create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<ProjectDto> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public ProjectDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public int getCount() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.getCount();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<ProjectDto> findAll(@NotNull final String userId, @NotNull final String sort) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<ProjectDto> comparator = sortType.getComparator();
            return projectRepository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDto findById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findByIdAndUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDto findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDto findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.removeByIdAndUserId(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @Nullable final ProjectDto project = findByIndex(userId, index);
            if (project == null) throw new EntityNotFoundException();
            @NotNull final IProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final ProjectDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        try {
            project.setName(name);
            project.setDescription(desc);
            @NotNull final IProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @Nullable final String desc
    ) {

        @Nullable final ProjectDto entity = findByIndex(userId, index);
        if (entity == null) throw new EntityNotFoundException();
        entity.setName(name);
        entity.setDescription(desc);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String strStatus
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final ProjectDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        try {
            Status status = Status.getStatus(strStatus);
            Map<String, Date> dateMap = prepareDates(status);
            project.setStatus(status);
            project.setStartDate(dateMap.get("start_date"));
            project.setEndDate(dateMap.get("end_date"));
            @NotNull final IProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String strStatus
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final ProjectDto project = findByName(userId, name);
        if (project == null) throw new EntityNotFoundException();
        try {
            Status status = Status.getStatus(strStatus);
            Map<String, Date> dateMap = prepareDates(status);
            project.setStatus(status);
            project.setStartDate(dateMap.get("start_date"));
            project.setEndDate(dateMap.get("end_date"));
            @NotNull final IProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String strStatus
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final ProjectDto project = findByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        try {
            Status status = Status.getStatus(strStatus);
            Map<String, Date> dateMap = prepareDates(status);
            project.setStatus(status);
            project.setStartDate(dateMap.get("start_date"));
            project.setEndDate(dateMap.get("end_date"));
            @NotNull final IProjectDtoRepository repository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isNotIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) return true;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return index >= projectRepository.getCountByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    private Map<String, Date> prepareDates(@NotNull final Status status) {
        Map<String, Date> map = new HashMap<>();
        switch (status) {
            case IN_PROGRESS:
                map.put("start_date", new Date());
                map.put("end_date", null);
                break;
            case COMPLETED:
                map.put("start_date", null);
                map.put("end_date", new Date());
                break;
            case NOT_STARTED:
                map.put("start_date", null);
                map.put("end_date", null);
        }
        return map;
    }

}
